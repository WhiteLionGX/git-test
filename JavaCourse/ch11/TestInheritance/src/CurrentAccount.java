public class CurrentAccount extends Account{

    public CurrentAccount(double balance, String name) {
        super(balance, name);
    }
    
    @Override
    public void addInterest() {
        setBalance( getBalance() * 1.1);
        System.out.println("Current account interest has been added.");
        System.out.println("New Balance: " + getBalance());
    }
}