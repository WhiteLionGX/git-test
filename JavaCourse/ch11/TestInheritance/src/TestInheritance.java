public class TestInheritance {
    public static void main(String[] args) throws Exception {
        
        Account[] arrayOfAccounts = new Account[3];
        arrayOfAccounts[0] = new CurrentAccount(2.0, "Bill");
        arrayOfAccounts[1] = new SavingsAccount(4.0, "Ted");
        arrayOfAccounts[2] = new SavingsAccount(6.0, "Zack");

        int i;

        for (i = 0; i < 3; i++) {
            int j = i + 1;
            
            System.out.println("Account Number: " + j);
            System.out.println("Name: " + arrayOfAccounts[i].getName());
            System.out.println("Balance: " + arrayOfAccounts[i].getBalance());
            arrayOfAccounts[i].addInterest();
            arrayOfAccounts[i].withdraw(1);
            System.out.println();
        }

    }
}
