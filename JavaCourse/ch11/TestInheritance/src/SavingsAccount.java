public class SavingsAccount extends Account{

    public SavingsAccount(double balance, String name) {
        super(balance, name);
    }
    
    @Override
    public void addInterest() {
        setBalance( getBalance() * 1.4);
        System.out.println("Savings account interest has been added.");
        System.out.println("New Balance: " + getBalance());
    }
}