public abstract class Account {

        private double balance;
        private String name;
    
        public double getBalance() {
            return balance;
        }
    
        public void setBalance(double balance) {
            this.balance = balance;
        }
    
        public String getName() {
            return name;
        }
    
        public void setName(String name) {
            this.name = name;
        }
    
        public abstract void addInterest();
    
        public Account(double balance, String name) {
            this.balance = balance;
            this.name = name;
        }
    
        public Account() {
            this.name = "John";
            this.balance = 50; 
        }
    
        public boolean withdraw (double amount){
            if (amount > balance){
                System.out.println("Error: User does not have enough money to withdraw " + amount + "$.");
                return false;
            } 
            else{
                balance = balance - amount;
                System.out.println(amount + "$ withdraw successful.");
                System.out.println("New Balance: " + balance);
                return true;
            }
        }
    
        public boolean withdraw(){
            if (20 > balance){
                System.out.println("Error: User does not have enough money to withdraw 20$.");
                return false;
            } 
            else{
                balance = balance - 20;
                System.out.println("20$ withdraw successful.");
                System.out.println("New Balance: " + balance);
                return true;
            }
        }
    }