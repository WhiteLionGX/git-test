public class TestAccount {

    public static void main(String[] args) {
        
        account[] arrayOfAccounts = new account[5];

        double[] amounts = { 10, 20, 30, 40, 50 };
        String[] names = { "John", "Jane", "Susan", "Mac", "Alex" };
        int i;

        for (i = 0; i < 5; i++) {
            int j = i + 1;
            arrayOfAccounts[i] = new account();
            arrayOfAccounts[i].setBalance(amounts[i]);
            arrayOfAccounts[i].setName(names[i]);
            System.out.println("Account Number: " + j);
            System.out.println("Name: " + arrayOfAccounts[i].getName());
            System.out.println("Balance: " + arrayOfAccounts[i].getBalance());
            arrayOfAccounts[i].addInterest();
            System.out.println("Balance (w/ Interest): " + arrayOfAccounts[i].getBalance());
            System.out.println();
        }
    }
}
