public class account {
    private double balance;
    private String name;
    private static double interestRate = 1.2;

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void addInterest() {
        balance = balance * interestRate;
        System.out.println("Interest has been added.");
        System.out.println("New Balance: " + balance);
    }

    public account(double balance, String name) {
        this.balance = balance;
        this.name = name;
    }

    public account() {
        this.name = "John";
        this.balance = 50; 
    }

    public static double getInterestRate() {
        return interestRate;
    }

    public static void setInterestRate(double interestRate) {
        account.interestRate = interestRate;
    }

    public boolean withdraw (double amount){
        if (amount > balance){
            System.out.println("Error: User does not have enough money to withdraw " + amount + "$.");
            return false;
        } 
        else{
            balance = balance - amount;
            System.out.println(amount + "$ withdraw successful.");
            System.out.println("New Balance: " + balance);
            return true;
        }
    }

    public boolean withdraw(){
        if (20 > balance){
            System.out.println("Error: User does not have enough money to withdraw 20$.");
            return false;
        } 
        else{
            balance = balance - 20;
            System.out.println("20$ withdraw successful.");
            System.out.println("New Balance: " + balance);
            return true;
        }
    }
}